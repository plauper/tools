/*
 * Maintenance script for:
 * - Database check
 * - Indizes Reorganize/Rebuild
 * - Rebuild Statistics
 */

SET NOCOUNT ON;  
DECLARE @dbs Table (name sysname)

-- Change to get all needed databases
INSERT INTO @dbs SELECT name FROM master.sys.databases where name like '%test%'
	AND state_desc = 'online' and is_read_only = 0

-- Change to customize reorganize or rebuild
DECLARE @min_pages       int = 10
DECLARE @min_reorganize  int = 5
DECLARE @min_rebuild     int = 25
DECLARE @offline_rebuild bit = 0

-- Declare the cursor for the list of databases to be processed.  
DECLARE @dbname sysname
DECLARE db_cursor CURSOR FOR SELECT name FROM @dbs;
OPEN db_cursor
WHILE (1=1)  
BEGIN
        FETCH NEXT FROM db_cursor   
        INTO @dbname;  
        IF @@FETCH_STATUS < 0 BREAK; 

		DECLARE @command nvarchar(max)

		SELECT @command = N'DBCC CHECKDB (' + @dbname + ') WITH NO_INFOMSGS'
		PRINT @command
		EXEC sp_executesql @command

		DECLARE @objectid int;  
		DECLARE @indexid int;  
		DECLARE @schemaname nvarchar(130);   
		DECLARE @objectname nvarchar(130);   
		DECLARE @indexname nvarchar(130);    
		DECLARE @frag float; 
		DECLARE @pages float;  

	-- Declare the cursor for the list of indizes to be processed.  
	DECLARE indizes_cursor CURSOR FOR SELECT  
			p.object_id
			,p.index_id
			,p.avg_fragmentation_in_percent
			,p.page_count
		FROM sys.dm_db_index_physical_stats (DB_ID(@dbname), NULL, NULL , NULL, null) AS p
  
	OPEN indizes_cursor 
	WHILE (1=1)  
	BEGIN
		FETCH NEXT FROM indizes_cursor INTO @objectid, @indexid, @frag, @pages;  
		IF @@FETCH_STATUS < 0 BREAK; 
			
		SELECT @command = N'SELECT @objectname = o.name, @indexname = i.name, @schemaname = s.name ' +
			N'FROM ' + @dbname + N'.sys.objects as o ' +
			N'JOIN ' + @dbname + N'.sys.schemas as s ON s.schema_id = o.schema_id ' +
			N'JOIN ' + @dbname + N'.sys.indexes i ON o.object_id = i.object_id AND i.index_id = ''' + CAST(@indexid AS sysname) + ''' ' +
			N'WHERE o.object_id = ''' + CAST(@objectid AS sysname) + ''''
			
		exec sp_executesql @command, 
                N'@objectname sysname output, @indexname sysname output, @schemaname sysname output'
				, @objectname output, @indexname output, @schemaname output

		SELECT @command = N''  
		IF (@pages >= @min_pages AND @frag >= @min_reorganize AND @frag < @min_rebuild)
		BEGIN
			SELECT @command = N'ALTER INDEX ' + @indexname + N' ON ' + @dbname + '.' + @schemaname + N'.' + @objectname + N' REORGANIZE'
		END
		ELSE IF (@pages >= @min_pages AND @frag >= @min_rebuild)
		BEGIN
			SELECT @command = N'ALTER INDEX ' + @indexname + N' ON ' + @dbname + '.' + @schemaname + N'.' + @objectname + N' REBUILD WITH (ONLINE = ON)' 
		END

		IF (LEN(@command) > 0)
		BEGIN
			PRINT N'  ' + @command
			BEGIN TRY
				EXEC sp_executesql @command
			END TRY
			BEGIN CATCH
			PRINT N'    ' + CAST(ERROR_NUMBER() AS nvarchar(20)) + N' ' + ERROR_MESSAGE()
			IF (@offline_rebuild = 1 AND (ERROR_NUMBER() = 2725 OR ERROR_NUMBER() = 1712)) -- online operation cannot be performed / can only be performed in Enterprise edition
			BEGIN
				SELECT @command = N'ALTER INDEX ' + @indexname + N' ON ' + @dbname + '.' + @schemaname + N'.' + @objectname + N' REBUILD'
				PRINT N'    ' + @command
				EXEC sp_executesql @command
			END
			END CATCH
		END
	END
	CLOSE indizes_cursor
	DEALLOCATE indizes_cursor
	
	-- Declare the cursor for the list of tables to be processed.  
	SELECT @command = 'DECLARE table_cursor CURSOR FOR SELECT TABLE_NAME, TABLE_SCHEMA FROM ' + @dbname + '.INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = ''BASE TABLE'''
	exec sp_executesql @command
	OPEN table_cursor
	WHILE (1=1)  
	BEGIN
        FETCH NEXT FROM table_cursor   
        INTO @objectname, @schemaname;  
        IF @@FETCH_STATUS < 0 BREAK; 

		SELECT @command = 'UPDATE STATISTICS ' + @dbname + '.' + @schemaname + '.' + @objectname + ' WITH FULLSCAN'
		PRINT N'  ' + @command
		EXEC sp_executesql @command
	END
	CLOSE table_cursor
	DEALLOCATE table_cursor

	PRINT ''

END
CLOSE db_cursor
DEALLOCATE db_cursor